// Instructions that can be provided to the students for reference:

// Activity:
// 1. In the S16 folder, create an activity folder, an index.html file inside of it and link the index.js file.
// 2. Create an index.js file and console log the message Hello World to ensure that the script file is properly associated with the html file.
// 3. Create two variables that will store two different number values and create several variables that will store the sum, difference, product and quotient of the two numbers. Print the results in the console
// 4. Using comparison operators print out a string in the console that will return a message if the sum is greater than the difference.
// 5. Using comparison and logical operators print out a string in the console that will return a message if the product and quotient are both positive numbers.
// 6. Using comparison and logical operators print out a string in the console that will return a message if one of the results is a negative number.
// 7. Using comparison and logical operators print out a string in the console that will return a message if one of the results is zero.


// Item 2.
console.log('Hello from JS');

// Item 3.
let x = 9
let y = 10
   // Sum
   let sum = x + y; 
   console.log('The sum of the two numbers is: ' + sum); // 19

   // Subtraction
   let subtract = x - y; 
   console.log('The difference of the two numbers is: ' + subtract); //-1

   // Multiplication
   let multiply = x * y; 
   console.log('The product of the two numbers is: ' + multiply);

   // Division
   let divide = x / y; 
   console.log('The quotient of the two numbers is: ' + divide);

// item 4. Using comparison operators print out a string in the console that will return a message if the sum is greater than the difference.
   
   let comparison = sum != subtract;
   console.log('The sum is greater than the difference: ' + comparison);

// Item 5. Using comparison and logical operators print out a string in the console that will return a message if the product and quotient are both positive numbers.

   let negative = false;
   let isComparison = multiply && divide != negative;
   console.log('The product and the quotient are positive numbers: ' + isComparison);


// Item 6. Using comparison and logical operators print out a string in the console that will return a message if one of the results is a negative number.
   let isAdditionAnspositive = true;
   let issubtractAnsNegative= true;
   let isResultNegative = isAdditionAnspositive || issubtractAnsNegative == issubtractAnsNegative;
   console.log('One of the result is negative: ' + isResultNegative);

// Item 7. Using comparison and logical operators print out a string in the console that will return a message if one of the results is zero.

   let result = sum && subtract && divide && multiply != 0;
   console.log('All the results are not equal to zero: ' + result);